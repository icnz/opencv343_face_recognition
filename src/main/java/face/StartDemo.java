package face;


import org.bytedeco.javacv.OpenCVFrameGrabber;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import face.business.CheckFacePoints;
import face.frame.ShowCamera;
import face.util.IdentityPeople;
/**
 * 启动程序
 * @author ShiQiang
 *
 */
@SpringBootApplication
@EnableAutoConfiguration
public class StartDemo { 
	public static void main(String[] args) throws Exception, InterruptedException {
		//SpringApplication.run (StartDemo.class, args);
		//1.训练人脸库
		IdentityPeople.train();
		//2.启动摄像头
		OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0);
		grabber.start(); // 开始获取摄像头数据 
		ShowCamera.init(grabber); 
		
		while (ShowCamera.close) {  
			/*摄像头显示*/
			//ShowCamera.show(CameraFaceInfo.dealTheMat(grabber.grabFrame())); 
			/*业务处理*/
			//ShowCamera.show(CheckCameraFace.dealTheMat(grabber.grabFrame()));
			/*摄像头上显示人脸关键点*/
			ShowCamera.show(CheckFacePoints.dealTheMat(grabber.grabFrame())); 
		}
	} 
} 