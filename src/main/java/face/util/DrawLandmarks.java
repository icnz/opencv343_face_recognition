package face.util;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.MatVector;
import org.bytedeco.javacpp.opencv_core.Point;
import org.bytedeco.javacpp.opencv_core.Point2fVector;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_imgproc;

public class DrawLandmarks {
	
	public static Scalar COLOR = new Scalar(255, 0,0 , 0);
	
	public static Mat drawPolyline(Mat img, Point2fVector landmarks, int start, int end, boolean isClosed) {
		// 收集开始和结束索引之间的所有点
		
		MatVector points = new MatVector(); 
		//此处有问题？？？？？？？？？？？？？？？？？？？？？？？？
//		List<MatOfPoint> pts = Lists.newArrayList();
		for (int i = start; i <= end; i++) {
			//points.put(landmarks.get()[i]);
			//Point p2f = new Point((int)landmarks.get()[i].x(),(int)landmarks.get()[i].y());
			points.push_back(new Mat((int)landmarks.get()[i].x(),(int)landmarks.get()[i].y()));
			//points.push_back(new PointMat(landmarks.get()[i]);
//			MatOfPoint e = new MatOfPoint();
//			e.fromNativeAddr(landmarks.get()[i].address());
//			pts.add(e);
			opencv_imgproc.putText(img,i+"", new Point((int)landmarks.get()[i].x(),(int)landmarks.get()[i].y()), opencv_imgproc.CV_FONT_HERSHEY_SIMPLEX,0.3, new Scalar(255, 0, 0, 0));
		}
	 
		 
		// 绘制多边形曲线参数	说明
//		img	作为画布的矩阵
//		pts	折线顶点数组
//		npts	折线顶点个数
//		ncontours	待绘制折线数
//		isClosed	是否是闭合折线(多边形)
//		color	折线的颜色
//		thickness	折线粗细
//		lineType	线段类型
//		shift	缩放比例(0是不缩放,4是1/4) 
		 
		//System.out.println(points);
		opencv_imgproc.polylines(img, points, isClosed, COLOR, 2, 16, 0);
		//org.opencv.core.Mat imgs = new org.opencv.core.Mat(img.rows(),img.cols(),img.type(),img.asByteBuffer());
		return img;
		//Imgproc.polylines(imgs,pts, isClosed,new org.opencv.core.Scalar(225,225,0));
		//opencv_imgproc.polylines(img, points, points.sizeof(), points.sizeof(), isClosed, COLOR, 1,1,1);

	}

	// 绘制人脸关键点
	public static Mat drawLandmarks(Mat im, Point2fVector landmarks) {
		// 在脸上绘制68点及轮廓（点的顺序是特定的，有属性的）
		if (landmarks.get().length == 68) {
			im = drawPolyline(im, landmarks, 0, 16, false); // Jaw line
			im = drawPolyline(im, landmarks, 17, 21, false); // Left eyebrow
			im = drawPolyline(im, landmarks, 22, 26, false); // Right eyebrow
			im = drawPolyline(im, landmarks, 27, 30, false); // Nose bridge
			im = drawPolyline(im, landmarks, 30, 35, true); // Lower nose
			im = drawPolyline(im, landmarks, 36, 41, true); // Left eye
			im = drawPolyline(im, landmarks, 42, 47, true); // Right Eye
			im = drawPolyline(im, landmarks, 48, 59, true); // Outer lip
			im = drawPolyline(im, landmarks, 60, 67, true); // Inner lip
		} else {
			// 如果人脸关键点数不是68，则我们不知道哪些点对应于哪些面部特征。所以，我们为每个landamrk画一个圆圈。
			for (int i = 0; i < landmarks.get().length; i++) {
				
				opencv_imgproc.circle(im,new Point((int) landmarks.get()[i].x(),(int) landmarks.get()[i].y()), 3, COLOR);
			}
		}
		return im;
	}
}
